# twitter clone
# models = posts, users
# users => id, email, token
# posts => id, content, user_id

from flask import Flask, request, g
from flask_restful import abort, Resource, Api
from peewee import *
from playhouse.shortcuts import model_to_dict
import uuid


app = Flask(__name__)
api = Api(app)


# Database config
db = SqliteDatabase('twitter.db')


# Database models
class BaseModel(Model):
    class Meta:
        database = db


class User(BaseModel):
    email = CharField()
    token = CharField()


class Tweet(BaseModel):
    content = CharField()
    user_id = IntegerField()


# Connect to database and create tables
db.connect()
db.create_tables([Tweet, User])


# Lookup tweet by ID
def get_tweet_by_id(tweet_id):
    try:
        return Tweet.get(id=tweet_id)
    except:
        abort(404, message='Tweet not found')

class API_Anakin(Resource):
    def get(self):
        longstring = """

        I'll try spinning that's a cool trick

        You're mine Dooku.

        My powers have doubled since the last time we met.
        It's not the Jedi way!
        YOU TURNED HER AGAINST ME!
        If you're not with me, then you are my enemy!
        You underestimate my power!
        """

class API_Tweets(Resource):
    def get(self):
        tweets = Tweet.select().where(Tweet.user_id == g.user)
        return [model_to_dict(task) for task in tasks]

    def post(self):
        if 'content' not in request.json.keys():
            abort(400, message='Please include content')
        #get the current user id using g object and add it to the Task record
        tweet = Tweet.create(content=request.json['content'], user_id=g.user.id)
        return model_to_dict(tweet)

class API_Tweet(Resource):
    def get(self, tweet_id):
        tweet = get_tweet_by_id(tweet_id)
        return model_to_dict(tweet)

    def patch(self, tweet_id):

        # Confirm user owns id
        tweet = get_tweet_by_id(tweet_id)

        # Update the content
        if 'content' in request.json.keys():
            query = Tweet.update(content=request.json['content']).where(Tweet.id == tweet_id)
            query.execute()

        # Get an updated copy
        tweet = get_tweet_by_id(tweet_id)
        return model_to_dict(tweet)

class API_User(Resource):
    def post(self):
        if 'email' not in request.json.keys():
            abort(400, message='Please include an email')
        else:
            user_token = uuid.uuid4()
            email_passed = email=request.json['email']
            print(email_passed)
            query = User.select().where(User.email == email_passed)
            if not query.exists():
                user = User.create(email=email_passed,token=str(user_token))
                return model_to_dict(user)
            else:
                abort(417,message="The user already exists.")


# Authenticate at the beginning of each request
@app.before_request
def before_request():
    
    if "register_user" not in str(request.__getattr__("url")):
        user_token = request.headers['Authorization']
        user = User.get(token=user_token)
        if (user == None):
            abort(401)
        else:
            g.user = user


# Routes
# api.add_resource(API_Tasks, '/tasks')
# api.add_resource(API_Task, '/tasks/<int:task_id>')
api.add_resource(API_Tweets, '/tweets')
api.add_resource(API_Tweet, '/tweet/<int:tweet_id>')
api.add_resource(API_User, '/register_user')
api.add_resource(API_Anakin, '/anakin')

if __name__ == '__main__':
    app.run(debug=True)